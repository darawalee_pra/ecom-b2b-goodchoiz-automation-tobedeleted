*** Settings ***
Library 	Selenium2Library
Resource 	${CURDIR}/../../../resources/keywords/ui/podapp/login.robot

*** Variables ***
${url}
${browser} 	Chrome

${invalid_username}
${invalid_password}

${valid_username}
${valid_password}

${username_field}
${password_field}
${login_bt}
${login_error_msg}
${shipping_list_driver}
${shipping_list_checker}
${shipping_list_finop}

*** Keywords ***


*** Test Cases ***
Login with invalid username/password
	Open browser 	${url} 	${browser}
	Input username 	${username_field} 	${invalid_username}
	Input password 	${password_field} 	${invalid_password}
	Click login button
	Verify login error message 	${login_error_msg}

Login with valid username/password - driver
	Open browser 	${url} 	${browser}
	Input username 	${username_field} 	${valid_username}
	Input password 	${password_field} 	${valid_password}
	Click login button
	Verify redirect to shipping list 	${shipping_list_driver}

Login with valid username/password - checker
	Open browser 	${url} 	${browser}
	Input username 	${username_field} 	${valid_username}
	Input password 	${password_field} 	${valid_password}
	Click login button
	Verify redirect to shipping list 	${shipping_list_checker}

Login with valid username/password - finop
	Open browser 	${url} 	${browser}
	Input username 	${username_field} 	${valid_username}
	Input password 	${password_field} 	${valid_password}
	Click login button
	Verify redirect to shipping list 	${shipping_list_finop}
