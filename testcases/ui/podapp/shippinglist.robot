*** Settings ***
Library 	Selenium2Library
Resource 	${CURDIR}/../../../resources/keywords/ui/podapp/shippinglist.robot

*** Variables ***
${confirm_shipno_bt}
${reject_shipno_bt}
${SO_3dots}
${shipno_3dots}
${3dots_option}
${remove_SO_bt}
${confirmation_modal}
${info_textfield}
${info_input_text}
${modal_cancel_bt}
${modal_ok_bt}
@{shipno_status} 	waiting 	confirm 	reject 	shipped
@{SO_status} 	waiting 	confirm 	remove 	shipped
${request_to_deliver_bt}
${SO_showmore_bt}
${SO_hide_bt}
${hamburger_bt}
${switch_language_bt}
${lang_TH}
${lang_EN}
${setting_ok_bt}
${setting_cancel_bt}
${logout_bt}


*** Keywords ***


*** Test Cases ***

Driver can confirm ship no by clicking confirm button
	Wait until page contains element 	${confirm_shipno_bt}
	Click element 	${confirm_shipno_bt}
	Wait until element is visible 	${confirmation_modal}
	Click element 	${modal_ok_bt}
	Page should not contain 	${confirm_shipno_bt}
	Verify element status 	${shipno_status} 	1

Driver can reject ship no by clicking confirm button
	Wait until page contains element 	${reject_shipno_bt}
	Click element 	${reject_shipno_bt}
	Wait until element is visible 	${confirmation_modal}
	Input text 	${info_textfield} 	${info_input_text}
	Click element 	${modal_ok_bt}
	Page should not contain 	${confirm_shipno_bt}
	Verify element status 	${shipno_status} 	2

Driver can confirm ship no by clicking 3 dots
	Wait until page contains element 	${shipno_3dots}
	Click element 	${shipno_3dots}
	Select option 	${3dots_option}
	Wait until element is visible 	${confirmation_modal}
	Input text 	${info_textfield} 	${info_input_text}
	Click element 	${modal_ok_bt}
	Verify element status 	${shipno_status} 	1

Driver can reject ship no by clicking 3 dots
	Wait until page contains element 	${shipno_3dots}
	Click element 	${shipno_3dots}
	Select option 	${3dots_option}
	Wait until element is visible 	${confirmation_modal}
	Input text 	${info_textfield} 	${info_input_text}
	Click element 	${modal_ok_bt}
	Verify element status 	${shipno_status} 	2

Driver can remove SO by clicking 3 dots
	Wait until page contains element 	${SO_3dots}
	Click element 	${SO_3dots}
	Select option 	${3dots_option}
	Wait until element is visible 	${confirmation_modal}
	Input text 	${info_textfield} 	${info_input_text}
	Click element 	${modal_ok_bt}
	Verify element status 	${SO_status} 	2

Driver can request to deliver by clicking request button
	Wait until page contains element 	${request_to_deliver_bt}
	Click element 	${request_to_deliver_bt}
	Wait until element is visible 	${confirmation_modal}
	Click element 	${modal_ok_bt}

Driver can see SO detail by clicking show more button
	Wait until page contains element 	${SO_showmore_bt}
	Click element 	${SO_showmore_bt}
	Page should contain element 	${SO_hide_bt}

Driver can close SO detail by clicking hide button
	Wait until page contains element 	${SO_hide_bt}
	Click element 	${SO_hide_bt}
	Page should contain element 	${SO_showmore_bt}

Driver can switch language in shipping list
	Wait until page contains element 	${hamburger_bt}
	Click element 	${hamburger_bt}
	Click element 	${switch_language_bt}
	Select language 	${lang_TH}
	Click element 	${setting_cancel_bt}
	Select language 	${lang_EN}
	Click element 	${setting_ok_bt}

Driver can logout from shipping list
	Wait until page contains element 	${hamburger_bt}
	Click element 	${hamburger_bt}
	Click element 	${logout_bt}
	Click element 	${setting_ok_bt}
	Wait until page does not contain element 	${hamburger_bt}



