*** Settings ***
Resource 	${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource 	${CURDIR}/../../../resources/keywords/api/podapp/shippinglist.robot
Resource 	${CURDIR}/../../../resources/keywords/api/podapp/login.robot


*** Variables ***
${title}
${developerMessage}
${driverId} 	1
${statusSO} 	3
${page} 	1
${perPage} 	10
${confirm_msg}
${reject_msg}
${shipping_err_msg} 	shipping list not found, please check your data!
${shipped_err_msg} 	shipped list not found, please check your data!

*** Keywords ***



*** Test Cases ***
Shipping - Verify error message is returned when request with not existing data - Driver
	[Tags] 	Shipping
	${response_shippinglist}= 	shippinglist.Get shipping list 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}
	Response Status Should Be 200 OK 	${response_shippinglist}
	Verify error msg of shipping list is returned 	${response_shippinglist} 	${shipping_err_msg}

Shipping - Verify shipping list is returned when request with existing data - Driver
	[Tags] 	Shipping
	${response_shippinglist}= 	shippinglist.Get shipping list 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}
	Response Status Should Be 200 OK 	${response_shippinglist}
	Verify correct shipping list is returned 	${response_shippinglist} 	${driverId}

Shipped - Verify error message is returned when request with not existing data - Driver
	[Tags] 	Shipped
	${response_shippedlist}= 	shippinglist.Get shipped list 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}
	Response Status Should Be 200 OK 	${response_shippedlist}
	Verify error msg of shipped list is returned 	${response_shippedlist} 	${shipped_err_msg}

Shipped - Verify shipped list is returned when request with existing data - Driver
	[Tags] 	Shipped
	${response_shippedlist}= 	shippinglist.Get shipped list 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}
	Response Status Should Be 200 OK 	${response_shippedlist}
	Verify correct shipped list is returned 	${response_shippedlist} 	${driverId}

Verify driver can confirm ship no
	${response_shippinglist}= 	shippinglist.Get shipping list 	${driverId} 	${day}
	${actual_shipToCode}= 	Verify correct shipping list is returned 	${response_shippinglist} 	${driverId}
	${response_confirm}= 	shippinglist.Post confirm ship no 	${actual_shipToCode}
	Verify ship no is confirmed 	${response_confirm} 	${confirm_msg}

Verify driver can reject ship no
	${response_shippinglist}= 	shippinglist.Get shipping list 	${driverId} 	${day}
	${actual_shipToCode}= 	Verify correct shipping list is returned 	${response_shippinglist} 	${driverId}
	${response_reject}= 	shippinglist.Post reject ship no 	${actual_shipToCode}
	Verify ship no is rejected 	${response_reject} 	${reject_msg}

