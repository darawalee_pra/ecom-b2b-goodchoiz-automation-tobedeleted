*** Settings ***
Resource 	${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource 	${CURDIR}/../../../resources/keywords/api/podapp/login.robot

#Test Setup 	Initial authentication service session


*** Variables ***
${firstname} 	darawalee
${lastname} 	praengthong
${error_message_incorrect} 	username or password is incorrect
${error_message_invalid} 	invalid username or password
${dev_error_message} 	Login Fail
${error_message_password} 	Passwords must be characters A-Z,a-z,0-9 or special characters, except Single, double, (), comma (,) and backslash (') are 8-16 digits.
${error_message_username} 	Username must be characters A-Z,a-z,0-9 can not be special characters more then 8 digits.
@{error_message_user_pass} 	Passwords must be characters A-Z,a-z,0-9 or special characters, except Single, double, (), comma (,) and backslash (') are 8-16 digits. 	Username must be characters A-Z,a-z,0-9 can not be special characters more then 8 digits.

*** Keywords ***
*** Test Cases ***
#User Service
Verify User Service return error message when request with invalid username/password
	[Template] 	login.Login with invalid username/password - User Service
	[Tags]  User
	User - username/password is not existing
	... 	${Login_dict} 	username_notexist 	password_notexist 	${dev_error_message}
	User - username is not existing
	... 	${Login_dict} 	username_notexist 	password_lower 	${dev_error_message}
	User - password is not existing
	... 	${Login_dict} 	username_lower 	password_notexist 	${error_message_incorrect}
	User - username/password is special char
	... 	${Login_dict} 	username_invalidspecial 	password_invalidspecial 	${error_message_username}
	User - username/password is in Thai
	... 	${Login_dict} 	username_Thai 	password_Thai 	${error_message_user_pass}
	User - username/password is less than minimum length
	... 	${Login_dict} 	username_UPPER 	password_lessthanmin 	${error_message_password}
	User - username/password is more than maximum length
	... 	${Login_dict} 	username_lower 	password_morethanmax 	${error_message_password}
	User - username/password is blank
	... 	${Login_dict} 	username_blank 	password_blank 	${error_message_user_pass}
	User - username is blank
	... 	${Login_dict} 	username_blank 	password_existing 	${error_message_username}
	User - password is blank
	... 	${Login_dict} 	username_existing 	password_blank 	${error_message_password}
	User - username/password is not sent
	... 	${Login_dict} 	username_notsendfield 	password_notsendfield 	${error_message_user_pass}

Verify User Serivce get user information when request with valid username/password
	[Template] 	login.Login with valid username/password - User Service
	[Tags]  User
	User - username/password is in UPPER case
	... 	${Login_dict} 	username_UPPER 	password_UPPER
	User - username/password is in lower case
	... 	${Login_dict} 	username_lower 	password_lower
	User - username/password is in upper+lower case
	... 	${Login_dict} 	username_existing 	password_existing

Verify the existing user can login after refresh token is expired but access token is still valid
	[Template] 	login.Login with valid username/password - User Service
	[Tags]  User
	User - username/password is in lower case
	... 	${Login_dict} 	username_lower 	password_lower
	User - user can login with the refresh token
	... 	${Login_dict} 	username_existing 	password_existing

Verify the existing user cannot login after access token is expired
	[Template] 	login.Login with valid username/password - User Service
	[Tags]  User
	User - user can login with new JWT token after it's expired
	... 	${Login_dict} 	username_lower 	password_lower
	User - user can login with new JWT token after it's expired
	... 	${Login_dict} 	username_existing 	password_existing

Verify User Service should call to Authorization service after received JWT token from Authentication service
	[Tags]  User
	${response_login} 	${actual_access_token} 	${actual_refresh_token}= 	login.Login with valid username/password - User Service 	Login_valid 	${Login_dict} 	username_UPPER 	password_UPPER
	Verify correct role is returned - User Service 	${response_login} 	${role_dict} 	6



#Authentication Service
Verify Authentication Service return error message when request with invalid username/password
	[Template] 	login.Login with invalid username/password - Authentication Service
	[Tags]  Authentication
	Authen - username/password is not existing
	... 	${Login_dict} 	username_notexist 	password_notexist 	${domain_dict} 	gz
	Authen - username/password is special char
	... 	${Login_dict} 	username_invalidspecial 	password_invalidspecial 	${domain_dict} 	gz
	Authen - username/password is in Thai
	... 	${Login_dict} 	username_Thai 	password_Thai 	${domain_dict} 	gz
	Authen - username/password is less than minimum length
	... 	${Login_dict} 	username_UPPER 	password_lessthanmin 	${domain_dict} 	gz
	Authen - username/password is more than maximum length
	... 	${Login_dict} 	username_lower 	password_morethanmax 	${domain_dict} 	gz
	Authen - username/password is blank
	... 	${Login_dict} 	username_blank 	password_blank 	${domain_dict} 	gz
	Authen - username/password is not sent
	... 	${Login_dict} 	username_notsendfield 	password_notsendfield 	${domain_dict} 	gz

Verify Authentication Serivce generate JWT token when request with valid username/password
	[Template] 	login.Login with valid username/password - Authentication Service
	[Tags]  Authentication
	Authen - username/password is in UPPER case
	... 	${Login_dict} 	username_UPPER 	password_UPPER 	${domain_dict} 	gz
	Authen - username/password is in lower case
	... 	${Login_dict} 	username_lower 	password_lower 	${domain_dict} 	gz
	Authen - username/password is in upper+lower case
	... 	${Login_dict} 	username_existing 	password_existing 	${domain_dict} 	gz

#Authorization Service
Verify Authorization Service return user role to user service
	[Tags]  Authorization
	${response_login}	${actual_access_token} 	${actual_refresh_token}=	Login with valid username/password - Authentication Service 	Login_valid 	${Login_dict} 	username_UPPER 	password_UPPER 	${domain_dict} 	gz
	Initial authorization service session
	${response_role}= 	login.Get user role 	${actual_access_token} 	${actual_refresh_token}
	Response Status Should Be 200 OK 	${response_role}
	Verify correct role is returned - Authentication Service 	${response_role} 	${role_dict}

Verify Authorization Service return user feature to user service
	[Tags]  Authorization
	${response_login}	${actual_access_token} 	${actual_refresh_token}=	Login with valid username/password - Authentication Service 	Login_valid 	${Login_dict} 	username_UPPER 	password_UPPER 	${domain_dict} 	gz
	Initial authorization service session
	${response_role}= 	login.Get user role 	${actual_access_token} 	${actual_refresh_token}
	Response Status Should Be 200 OK 	${response_role}
	Verify correct role is returned - Authentication Service 	${response_role} 	${role_dict}
	${response_permission}= 	login.Get user permission 	${actual_access_token} 	${actual_refresh_token}
	Verify correct permission is returned 	${response_permission} 	${feature_dict}