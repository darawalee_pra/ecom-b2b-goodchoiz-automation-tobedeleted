*** Variables ***
##alpha##

# Login
&{Login_dict}
... 	username_notexist=Notexisting1
... 	username_invalidspecial=User$%^&*(
... 	username_UPPER=PODUSERNAME1
... 	username_lower=podtest1
... 	username_Thai=ยูสเซอร์เนม
... 	username_blank=${EMPTY}
... 	username_notsendfield=
... 	username_existing=QAtestpod01
... 	password_notexist=Notpassword
... 	password_invalidspecial=pass'word
... 	password_UPPER=PODPASSWORD1
... 	password_lower=podpassword1
... 	password_number=12345678
... 	password_Thai=พาสเวิร์ด
... 	password_blank=${EMPTY}
... 	password_notsendfield=
... 	password_lessthanmin=123
... 	password_morethanmax=passwordis1234567890
... 	password_existing=password!@#$%^

&{role_dict}
... 	4=admin
... 	5=fin_ofs
... 	6=driver
... 	7=checker

&{feature_dict}
... 	5=all
... 	6=manage_user
... 	7=manage_role
... 	8=no_permission
... 	23=checking_shipped
... 	24=checking_shiping
... 	28=approve_shipping
... 	30=approve_shipped
... 	21=update_shipping
... 	22=update_shipped
... 	27=scan_qr

&{domain_dict}
... 	gz=@goodchoiz.com



# API
${api_root_url} 	http://192.168.10.237:8080

### User Service ###
${user_service} 	/starter
${login_url} 	/api/v1/user/loginUser


### Authentication Service ###
${authentication_service} 	/authentication
${authen_url} 	/v1/tokens

### Authorization Service ###
${authorization_service} 	/authorization
${author_feature_url} 	/v2/features
${author_role_url} 	/v2/roles

### Shipping List ###
${shipping_list} 	/api
${shipping_url} 	/v1/shippinglist

### Shipped List ###
${shipped_list} 	/api
${shipped_url} 	/v1/shippedlist