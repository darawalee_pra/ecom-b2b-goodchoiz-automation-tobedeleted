*** Variables ***
##prod##

# API
${api_root_url} 	http://production

### User Service ###
${user_service} 	/user
${login_url} 	/login


### Authentication Service ###
${authentication_service} 	/authentication-api-1.0-SNAPSHOT
${authen_url} 	/v1/tokens

### Authorization Service ###

