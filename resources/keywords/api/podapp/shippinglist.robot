*** Settings ***
Library 	RequestsLibrary
Library 	Collections
Library 	JSONLibrary
Resource 	${CURDIR}/../../../resources/config/${ENV}/config.robot
Resource 	${CURDIR}/../../../resources/keywords/api/podapp/login.robot

*** Variables ***
${api_shipping_list_session} 	api_shipping_list_session
${api_shipped_list_session} 	api_shipped_list_session

*** Keywords ***
Initial shipping list session

	RequestsLibrary.Create Session 	alias=${api_shipping_list_session} 	url=${api_root_url}${shipping_list} 	verify=${False} 	timeout=30

Get shipping list
	[Arguments] 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}

	&{header_Dict}=	Create dictionary 	custId=2496 	Content-Type=application/x-www-form-urlencoded
	&{param_Dict}=	Create dictionary 	driverId=${driverId} 	day=${day} 	statusSO=${statusSO} 	page=${page} 	perPage=${perPage}

	${response_shippinglist}= 	RequestsLibrary.Post Request 	alias=${api_shipping_list_session} 	uri=${shipping_url} 	headers=&{header_Dict} 	params=&{params_Dict} 	timeout=10

	[Return] 	${response_shippinglist}

Get shipped list
	[Arguments] 	${driverId} 	${day} 	${statusSO} 	${page} 	${perPage}

	&{header_Dict}=	Create dictionary 	custId=2496 	Content-Type=application/x-www-form-urlencoded
	&{param_Dict}=	Create dictionary 	driverId=${driverId} 	day=${day} 	statusSO=${statusSO} 	page=${page} 	perPage=${perPage}

	${response_shippedlist}= 	RequestsLibrary.Post Request 	alias=${api_shipped_list_session} 	uri=${shipped_url} 	headers=&{header_Dict} 	params=&{params_Dict} 	timeout=10

	[Return] 	${response_shippinglist}

Verify correct shipping list is returned
#check driverId for all shipto is corrected and check shiptocode item = totalitem
	[Arguments] 	${response_shippinglist} 	${driverId}

	${actual_driverId}= 	JSONLibrary.Get Value From Json 	${response_shippinglist.json()} 	$..driverId
	${length}= 	Get length 	${actual_driverId}
	:FOR 	${index} 	IN 	${length}
	\	Should be equal 	${actual_driverId}[${index}] 	${driverId}
	${actual_shipToCode}= 	JSONLibrary.Get Value From Json 	${response_shippinglist.json()} 	$..shipToCode
	Remove duplicates 	${actual_shipToCode}
	${ship_length}= 	Get length 	${actual_shipToCode}
	${actual_totalItems}= 	JSONLibrary.Get Value From Json 	${response_shippinglist.json()} 	$.totalItems
	Should be equal 	@{actual_totalItems}[0] 	${ship_length}

	[Return] 	${actual_shipToCode}

Verify correct shipped list is returned
#check driverId for all shipto is corrected and check shiptocode item = totalitem
	[Arguments] 	${response_shippedlist} 	${driverId}

	${actual_driverId}= 	JSONLibrary.Get Value From Json 	${response_shippedlist.json()} 	$..driverId
	${length}= 	Get length 	${actual_driverId}
	:FOR 	${index} 	IN 	${length}
	\	Should be equal 	${actual_driverId}[${index}] 	${driverId}
	${actual_shipToCode}= 	JSONLibrary.Get Value From Json 	${response_shippedlist.json()} 	$..shipToCode
	Remove duplicates 	${actual_shipToCode}
	${ship_length}= 	Get length 	${actual_shipToCode}
	${actual_totalItems}= 	JSONLibrary.Get Value From Json 	${response_shippedlist.json()} 	$.totalItems
	Should be equal 	@{actual_totalItems}[0] 	${ship_length}

	[Return] 	${actual_shipToCode}

Verify error msg of shipping list is returned
	[Arguments] 	${response_shippinglist} 	${shipping_err_msg}
	${actual_shipping_err_msg}= 	JSONLibrary.Get Value From Json 	${response_shippinglist.json()} 	$.developerMessage
	Should be equal 	@{actual_shipping_err_msg}[0] 	${shipping_err_msg}

Verify error msg of shipped list is returned
	[Arguments] 	${response_shippedlist} 	${shipped_err_msg}
	${actual_shipped_err_msg}= 	JSONLibrary.Get Value From Json 	${response_shippedlist.json()} 	$.developerMessage
	Should be equal 	@{actual_shipped_err_msg}[0] 	${shipped_err_msg}
