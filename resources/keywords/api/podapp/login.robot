*** Settings ***
Library 	RequestsLibrary
Library 	JSONLibrary
Resource 	${CURDIR}/../../../config/${ENV}/config.robot
Resource 	${CURDIR}/../apicommon.robot

*** Variables ***
${api_user_service_session} 	user_service_session
${api_authen_service_session} 	authen_service_session

*** Keywords ***
Initial user service session
	[Arguments] 	${username} 	${password}

	${auth}= 	create list 	${username} 	${password}
	RequestsLibrary.Create Session 	alias=${api_user_service_session} 	url=${api_root_url}${user_service} 	auth=${auth} 	verify=${False} 	timeout=30

Initial authentication service session
	[Arguments] 	${username} 	${password}

	${auth}= 	create list 	${username} 	${password}
	RequestsLibrary.Create Session 	alias=${api_authen_service_session} 	url=${api_root_url}${authentication_service} 	auth=${auth} 	verify=${False} 	timeout=30

Initial authorization service session

	RequestsLibrary.Create Session 	alias=${api_authen_service_session} 	url=${api_root_url}${authorization_service} 	verify=${False} 	timeout=30


Login user
	[Arguments] 	${username} 	${password}

	${username}= 	Encode String To Bytes 	${username} 	UTF-8
	${password}= 	Encode String To Bytes 	${password} 	UTF-8

	Initial user service session 	${username} 	${password}

	&{header_Dict}= 	create dictionary 	Content-Type=application/json
	&{body_Dict}= 	create dictionary 	username=${username} 	password=${password}

	${response}= 	RequestsLibrary.Post Request 	alias=${-----} 	uri=${login_url} 	headers=&{header_Dict} 	data=&{body_Dict} 	timeout=10

	[Return] 	${response}

Login authen
	[Arguments] 	${username} 	${password}

	${username}= 	Encode String To Bytes 	${username} 	UTF-8
	${password}= 	Encode String To Bytes 	${password} 	UTF-8

	Initial authentication service session 	${username} 	${password}

	&{header_Dict}= 	create dictionary 	Authorization=Basic eW9rYW1AZy5jb206Z2dnZ2dn 	Content-Type=application/x-www-form-urlencoded
	&{body_Dict}= 	create dictionary 	grant_type=member

	${response}= 	RequestsLibrary.Post Request 	alias=${api_authen_service_session} 	uri=${authen_url} 	headers=&{header_Dict} 	data=&{body_Dict} 	timeout=10
	Log 	${response.json()}

	[Return] 	${response}

Get user role
	[Arguments] 	${actual_access_token} 	${actual_refresh_token}

	&{header_Dict}= 	create dictionary 	x-gz-accessToken=${actual_access_token} 	x-gz-refreshToken=${actual_refresh_token} 	Content-Type=application/x-www-form-urlencoded

	${response} 	RequestsLibrary.Get Request 	alias=${api_authen_service_session} 	uri=${author_role_url} 	headers=&{header_Dict} 	timeout=10
	Log To Console	response role: ${response}

	[Return] 	${response}

Get user permission
	[Arguments] 	${actual_access_token} 	${actual_refresh_token}

	&{header_Dict}= 	create dictionary 	x-gz-accessToken=${actual_access_token} 	x-gz-refreshToken=${actual_refresh_token}

	${response} 	RequestsLibrary.Get Request 	alias=${api_authen_service_session} 	uri=${author_feature_url} 	headers=&{header_Dict} 	timeout=10
	Log To Console	response feature: ${response}

	[Return] 	${response}


Login with valid username/password - User Service
	[Arguments] 	${TC_name} 	${Login_List} 	${username} 	${password}
	${valid_username}=	Get From Dictionary 	${Login_List} 	${username}
	${valid_password}=	Get From Dictionary 	${Login_List} 	${password}
	${response_login}= 	Login user 	${valid_username} 	${valid_password}
	Response Status Should Be 200 OK 	${response_login}
	${actual_access_token} 	${actual_refresh_token}= 	Verify JWT token is returned - User Service 	${response_login}
	[Return] 	${response_login} 	${actual_access_token} 	${actual_refresh_token}

Login with invalid username/password - User Service
	[Arguments] 	${TC_name} 	${Login_List} 	${username} 	${password} 	${error}
	${valid_username}=	Get From Dictionary 	${Login_List} 	${username}
	${valid_password}=	Get From Dictionary 	${Login_List} 	${password}
	${response_login}= 	Login user 	${valid_username} 	${valid_password}
	Verify error message is returned - User Service 	${response_login} 	${error}
	[Return]	${response_login}

Login with valid username/password - Authentication Service
	[Arguments] 	${TC_name} 	${Login_List} 	${username} 	${password} 	${domain_dict} 	${domain}
	${valid_username}=	Get From Dictionary 	${Login_List} 	${username}
	${valid_password}=	Get From Dictionary 	${Login_List} 	${password}
	${valid_domain}=	Get From Dictionary 	${domain_dict} 	${domain}
	${valid_username_and_domain}= 	Set Variable 	${valid_username}${valid_domain}
	${response_login}= 	Login authen 	${valid_username_and_domain} 	${valid_password}
	Response Status Should Be 200 OK 	${response_login}
	${actual_access_token} 	${actual_refresh_token}= 	Verify JWT token is returned - Authentication Service 	${response_login}
	[Return] 	${response_login} 	${actual_access_token} 	${actual_refresh_token}

Login with invalid username/password - Authentication Service
	[Arguments] 	${TC_name} 	${Login_List} 	${username} 	${password} 	${domain_dict} 	${domain}
	${valid_username}=	Get From Dictionary 	${Login_List} 	${username}
	${valid_password}=	Get From Dictionary 	${Login_List} 	${password}
	${valid_domain}=	Get From Dictionary 	${domain_dict} 	${domain}
	${valid_username_and_domain}= 	Set Variable 	${valid_username}${valid_domain}
	${response_login}= 	Login authen 	${valid_username_and_domain} 	${valid_password}
	Response Status Should Be 401 Unauthorized 	${response_login}
	Verify error message is returned - Authentication Service 	${response_login} 	${error_message_invalid}
	[Return]	${response_login}


Verify correct role is returned - User Service
	[Arguments] 	${response} 	${role_dict} 	${index}
	${actual_role}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..role
	${expected_role}= 	Get From Dictionary 	${role_dict} 	${index}
	Should Be Equal 	@{actual_role}[0] 	${expected_role}

Verify correct role is returned - Authentication Service
	[Arguments] 	${response} 	${role_dict}
	${actual_role_dict}=	Create Dictionary
	${actual_id_list}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..id
	${actual_role_list}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..name
	${length}= 	Get Length 	${actual_id_list}
	:FOR 	${index} 	IN RANGE 	0 	${length}
	\	${actual_id}= 	Get From List 	${actual_id_list} 	${index}
	\	${actual_id}= 	Convert To String 	${actual_id}
	\	${actual_role}= 	Get From List 	${actual_role_list} 	${index}
	\ 	Set To Dictionary 	${actual_role_dict} 	${actual_id}	${actual_role}
	Should Be Equal 	${actual_role_dict} 	${role_dict}

Verify correct permission is returned
	[Arguments] 	${response} 	${feature_dict}
	${actual_feature_dict}=	Create Dictionary
	${actual_id_list}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..id
	${actual_feature_list}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..name
	${length}= 	Get Length 	${actual_id_list}
	:FOR 	${index} 	IN RANGE 	0 	${length}
	\	${actual_id}= 	Get From List 	${actual_id_list} 	${index}
	\	${actual_id}= 	Convert To String 	${actual_id}
	\	${actual_feature}= 	Get From List 	${actual_feature_list} 	${index}
	\	${actual_feature}= 	Convert To String 	${actual_feature}
	\ 	Set To Dictionary 	${actual_feature_dict} 	${actual_id}	${actual_feature}
#	Log Dictionary 	${actual_feature_dict}
#	Log Dictionary 	${feature_dict}
	Should Be Equal 	${actual_feature_dict} 	${feature_dict}

Verify correct firstname is returned
	[Arguments] 	${response} 	${firstname}

	${actual_firstname}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$.firstname
	Log To Console 	${actual_firstname}
	BuiltIn.Should Be Equal 	@{actual_firstname}[0] 	${firstname}

Verify correct lastname is returned
	[Arguments] 	${response} 	${lastname}

	${actual_lastname}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$.lastname
	Log To Console 	${actual_lastname}
	BuiltIn.Should Be Equal 	@{actual_lastname}[0] 	${lastname}

Verify JWT token is returned - User Service
	[Arguments] 	${response}

	${actual_access_token}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..accessToken
	${actual_refresh_token}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..refreshToken
	${actual_access_token}= 	Get From List 	${actual_access_token} 	0
	${actual_refresh_token}= 	Get From List 	${actual_refresh_token} 	0
	[Return] 	${actual_access_token} 	${actual_refresh_token}

Verify JWT token is returned - Authentication Service
	[Arguments] 	${response}

	${actual_access_token}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..access_token
	${actual_refresh_token}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$..refresh_token
	${actual_access_token}= 	Get From List 	${actual_access_token} 	0
	${actual_refresh_token}= 	Get From List 	${actual_refresh_token} 	0
	[Return] 	${actual_access_token} 	${actual_refresh_token}

Verify error message is returned - User Service
	[Arguments] 	${response} 	${expected_error_msg}

	${response_code}= 	Set Variable 	${response.status_code}

	${actual_response}= 	run keyword if 	'${response_code}' == '400' 	JSONLibrary.Get Value From Json 	${response.json()} 	$['errors'][*]['message']
	... 	ELSE IF 	'${response_code}' == '200' 	JSONLibrary.Get Value From Json 	${response.json()} 	$.developerMessage
	${response_length}= 	Get Length 	${actual_response}
	Run Keyword If 	'${response_length}' == '1' 	Should Be Equal 	@{actual_response}[0] 	${expected_error_msg}
	:FOR 	${index} 	IN RANGE 	0	${response_length}
	\	${expected_error}= 	Run Keyword If 	'${response_length}' != '1' 	Get From List 	${expected_error_msg} 	${index} 	ELSE 	Exit For Loop
	\	List Should Contain Value 	${actual_response} 	${expected_error}
	Log To Console 	${\n} error code: ${response_code} ${\n} error msg: ${actual_response}

Verify error message is returned - Authentication Service
	[Arguments] 	${response} 	${expected_error_msg}

	${actual_response}= 	JSONLibrary.Get Value From Json 	${response.json()} 	$.errorMessage
	BuiltIn.Should Be Equal 	@{actual_response}[0] 	${expected_error_msg}
	Log To Console 	error msg: ${actual_response}

