#########################################################################
# Page Object Name : Global keywords for API 
# Writer : Darawalee P
# Date : 16-Aug-2018
# Dependencies : N/A
#########################################################################
*** Settings ***
Library 	Collections
Library 	DatabaseLibrary
Library 	DateTime
Library 	JSONLibrary
Library 	RequestsLibrary
Library 	String

*** Keywords ***
##### HTTP STATUS CODES ASSERTION #####
Response Status Should Be 200 OK
	[Arguments]	${response}
	Should Be Equal As Integers 	200 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 201 Created
	[Arguments]	${response}
	Should Be Equal As Integers 	201 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 204 No Content
	[Arguments]	${response}
	Should Be Equal As Integers 	204 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 400 Bad Request
	[Arguments]	${response}
	Should Be Equal As Integers 	400 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}


Response Status Should Be 401 Unauthorized
	[Arguments]	${response}
	Should Be Equal As Integers 	401 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 404 Not Found
	[Arguments]	${response}
	Should Be Equal As Integers 	404 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 403 Forbidden
	[Arguments]	${response}
	Should Be Equal As Integers 	403 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 405 Method Not Allowed
	[Arguments]	${response}
	Should Be Equal As Integers 	405 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 409 Conflict
	[Arguments]	${response}
	Should Be Equal As Integers 	409 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}

Response Status Should Be 500 Internal Server Error
	[Arguments]	${response}
	Should Be Equal As Integers 	500 	${response.status_code}
	Log To Console 	Response Status: ${response.status_code}


