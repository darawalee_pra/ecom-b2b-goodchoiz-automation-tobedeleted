*** Settings ***
Library 	Selenium2Library

*** Variables ***
${login_error_location}

*** Keywords ***
Open browser
	[Arguments] 	${url} 	${browser}
	Open browser 	${url} 	${browser}

Input username
	[Arguments] 	${field} 	${text}
	Clear element text 	${field}
	Input text 	${field} 	${text}

Input password
	[Arguments] 	${field} 	${text}
	Clear element text 	${field}
	Input password 	${field} 	${text}

Click login button
	[Arguments] 	${button}
	Wait for element is visible 	${button} 	timeout=10
	Click element 	${button}

Verify login error message
	[Arguments] 	${login_error_msg}
	${actual_login_error_msg}=	Get text 	${login_error_location}
	Should be equal 	${actual_login_error_msg} 	${login_error_msg}

Verify redirect to shipping list
	[Arguments] 	${shipping_list_role}
	Wait until page contains element 	${shipping_list_role}
